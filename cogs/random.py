from discord.ext import commands
import asyncio
import random

class Random(commands.Cog):

    def __init__(self, client):
        self.client = client
    
    @commands.command(brief='Sends happy matt or unhappy matt, randomly chosen')
    async def emoji(self, ctx):
        options = ["downmatt", "happymatt"]
        emote = random.choice(options)
        async with ctx.channel.typing():
            await asyncio.sleep(1)
        emoji = [x for x in ctx.guild.emojis if x.name == emote]
        await ctx.channel.send(f"<:{emoji[0].name}:{emoji[0].id}>")
    
    @commands.command(brief='roll some dice for House Games')
    async def roll(self, ctx, num: int, dice: int, difficulty: int=None, botch: bool=True):
        results = [random.randint(1,dice) for i in range(num)]
        try:
            successes = 0
            for i in results:
                if i >= difficulty:
                    successes += 1
                elif botch == True and i == 1:
                    successes -= 1

            async with ctx.channel.typing():
                await asyncio.sleep(1)
            await ctx.channel.send(f"You rolled: {results} with **{successes}** successes")

        except:
            async with ctx.channel.typing():
                await asyncio.sleep(1)
            await ctx.channel.send(f"You rolled: {results}")

    @commands.command(brief='choose random item from a comma separated list')
    async def random(self, ctx, *, input: str):
        options = input.split(",")
        async with ctx.channel.typing():
            await asyncio.sleep(1)
        await ctx.channel.send(f"{random.choice(options)}")

def setup(client):
    client.add_cog(Random(client))
