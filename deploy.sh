#! /bin/bash

# Check if a docker container is running already with the name discord-bot
if docker ps -a | grep discord-bot > /dev/null ; then
    OLD_IMAGE=$(docker ps -a | grep discord-bot | awk '{print $2}')
    docker rm -f discord-bot
    docker rmi ${OLD_IMAGE}
fi

# Deploy the updated version of the bot

docker run -d -e {{BOT_TOKEN}} -e {{CANNON_FODDER}} -e {{TMT_MEMBER}} -e {{BENNY}} -e {{MATT}} --name discord-bot {{IMAGE}}